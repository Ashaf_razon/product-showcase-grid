class ProductDataModel{
  //variables
  int? id;
  String? name;
  String? description;
  String? imageURL;
  String? oldPrice;
  String? price;

  //constructor with required param
  ProductDataModel(
      {
        this.id,
        this.name,
        this.description,
        this.imageURL,
        this.oldPrice,
        this.price
      });

  //maps variables to json product data
  ProductDataModel.fromJson(Map<String,dynamic> json)
  {
    id = json['id'];
    name =json['name'];
    description = json['description'];
    imageURL = json['imageUrl'];
    oldPrice = json['oldPrice'];
    price = json['price'];
  }
}