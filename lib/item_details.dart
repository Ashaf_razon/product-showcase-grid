import 'package:flutter/material.dart';
import 'package:flutter_product_show/product_data_model.dart';
import 'package:get/get.dart';
import 'main.dart';

class ItemDetails extends StatelessWidget {
  ItemDetails({Key? key}) : super(key: key);
  final int index = Get.arguments ?? 0;
  int? productPrice;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Item Details')),
      body: detailsBodyHome(),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            debugPrint('getPrice: '+productPrice.toString());
            Get.snackbar(
              "Successfully added to the cart",
              "Cart cost: "+productPrice.toString(),
              backgroundColor: Colors.pink,
              borderRadius: 20,
              margin: const EdgeInsets.all(15),
              colorText: Colors.white,
              duration: const Duration(seconds: 4),
              icon: const Icon(Icons.add_shopping_cart_sharp, color: Colors.white),
              snackPosition: SnackPosition.BOTTOM,

            );
          },
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        )
    );
  }

  detailsBodyHome() {
    return FutureBuilder(
        future: readJsonData(),
        builder: (context, data) {
          if (data.hasError) {
            return Center(child: Text("${data.error}"));
          } else if (data.hasData) {
            //global var inintialization
            var items = data.data as List<ProductDataModel>;
            productPrice = int.parse(items[index].price.toString());

            return Card(
              elevation: 3,
              margin:
                  const EdgeInsets.all(20),
              child: Container(
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 160,
                      height: 200,
                      child: Image(
                        image:
                            NetworkImage(items[index].imageURL.toString()),
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 2, right: 2),
                            child: Text(
                              items[index].name.toString(),
                              style: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 2, right: 2),
                            child: Text('Price: '+productPrice.toString()),
                          ),
                          Padding(
                            padding:
                            const EdgeInsets.only(left: 2, right: 2),
                            child: Text('Description: ' +
                                items[index].description.toString()),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}